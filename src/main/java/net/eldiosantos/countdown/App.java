package net.eldiosantos.countdown;

import com.google.gson.Gson;
import net.eldiosantos.countdown.bean.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;

import static spark.Spark.*;

/**
 * Hello world!
 */
public class App {

    private static final DateTime eldiusHolidayStart;

    static {
        eldiusHolidayStart = new DateTime(2015, 10, 14, 19, 0, 0, 0, DateTimeZone.forOffsetHours(-3));
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");

        final Gson gson = new Gson();

        port(Integer.valueOf(System.getenv("PORT") != null ? System.getenv("PORT") : "9999"));

        after((req, res) -> res.type("application/json"));

        get("/eldius", (req, res) -> {
            final Response instance = Response.getInstance(new Period(DateTime.now(), eldiusHolidayStart.toInstant()));
            res.header("time", instance.toString());

            return gson.toJson(instance);
        });
    }
}
