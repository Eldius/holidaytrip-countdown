package net.eldiosantos.countdown.bean;

import org.joda.time.Period;

/**
 * Created by esjunior on 12/08/2015.
 */
public class Response {
    private Integer milis;
    private Integer secs;
    private Integer mins;
    private Integer hours;
    private Integer days;
    private Integer months;
    private Integer years;

    public Response() {
    }

    public Response(Integer milis, Integer secs, Integer mins, Integer hours, Integer days, Integer mouths, Integer years) {
        this.milis = milis;
        this.secs = secs;
        this.mins = mins;
        this.hours = hours;
        this.days = days;
        this.months = mouths;
        this.years = years;
    }

    public Integer getMilis() {
        return milis;
    }

    public Response setMilis(Integer milis) {
        this.milis = milis;
        return this;
    }

    public Integer getSecs() {
        return secs;
    }

    public Response setSecs(Integer secs) {
        this.secs = secs;
        return this;
    }

    public Integer getMins() {
        return mins;
    }

    public Response setMins(Integer mins) {
        this.mins = mins;
        return this;
    }

    public Integer getHours() {
        return hours;
    }

    public Response setHours(Integer hours) {
        this.hours = hours;
        return this;
    }

    public Integer getDays() {
        return days;
    }

    public Response setDays(Integer days) {
        this.days = days;
        return this;
    }

    public Integer getMonths() {
        return months;
    }

    public Response setMonths(Integer months) {
        this.months = months;
        return this;
    }

    public Integer getYears() {
        return years;
    }

    public Response setYears(Integer years) {
        this.years = years;
        return this;
    }

    public static Response getInstance(final Period period) {
        final Response response = new Response();

        response.setYears(period.getYears());
        period.minusYears(period.getYears());

        response.setMonths(period.getMonths());
        period.minusMonths(period.getMonths());

        response.setDays(period.getDays());
        period.minusDays(period.getDays());

        response.setHours(period.getHours());
        period.minusHours(period.getHours());

        response.setMins(period.getMinutes());
        period.minusMinutes(period.getMinutes());

        response.setSecs(period.getSeconds());
        period.minusSeconds(period.getSeconds());

        response.setMilis(period.getMillis());
        period.minusMillis(period.getMillis());

        System.out.println(String.format("empty period: %s", period.getMillis()));

        return response;
    }

    @Override
    public String toString() {
        return String.format(
                "it will happen in %d years, %d months, %d days, %d hours, %d minutes, %d seconds and %d millisseconds"
                , this.getYears()
                , this.getMonths()
                , this.getDays()
                , this.getHours()
                , this.getMins()
                , this.getSecs()
                , this.getMilis()
            );
    }
}
